using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumTemplate.Queries;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps
{
    public class CriarCampoPersonalizadoDBSteps
    {
        public static void CriarCampoPersonalizado()
        {
            string query = File.ReadAllText(GeneralHelpers.GetProjectPath() + "Queries/GerenciarQueries/CriarCampoPersonalizado.sql");

            DataBaseHelpers.ExecuteQuery(query);
        }
    }
} 