using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumTemplate.Queries;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps
{
    public class CriarUsuarioDBSteps
    {
        public static void CriarUsuario()
        {
            string query = File.ReadAllText(GeneralHelpers.GetProjectPath() + "Queries/GerenciarQueries/CriarUsuario.sql");

            DataBaseHelpers.ExecuteQuery(query);
        }
    }
}