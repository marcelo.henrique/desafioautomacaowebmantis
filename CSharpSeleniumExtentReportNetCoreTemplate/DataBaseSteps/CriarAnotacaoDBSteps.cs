using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumTemplate.Queries;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps
{
    public class CriarAnotacaoDBSteps
    {
        public static void CriarAnotacao()
        {
            string query = File.ReadAllText(GeneralHelpers.GetProjectPath() + "Queries/TarefaQueries/CriarAnotacao.sql");

            DataBaseHelpers.ExecuteQuery(query);
        }
    }
} 