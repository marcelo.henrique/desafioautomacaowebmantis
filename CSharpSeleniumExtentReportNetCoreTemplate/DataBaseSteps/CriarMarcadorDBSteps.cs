using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumTemplate.Queries;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps
{
    public class CriarMarcadorDBSteps
    {
        public static void CriarMarcador()
        {
            string query = File.ReadAllText(GeneralHelpers.GetProjectPath() + "Queries/GerenciarQueries/CriarMarcador.sql");

            DataBaseHelpers.ExecuteQuery(query);
        }
    }
} 