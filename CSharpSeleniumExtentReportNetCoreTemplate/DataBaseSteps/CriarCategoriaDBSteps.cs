using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumTemplate.Queries;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps
{
    public class CriarCategoriaDBSteps
    {
        public static void CriarCategoria()
        {
            string query = File.ReadAllText(GeneralHelpers.GetProjectPath() + "Queries/GerenciarQueries/CriarCategoria.sql");

            DataBaseHelpers.ExecuteQuery(query);
        }
    }
} 