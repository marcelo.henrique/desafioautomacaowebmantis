using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class LoginPage : PageBase
    {
        #region Mapping
        By nomeDoUtilizador = By.Id("username");
        By palavraChave = By.Id("password");
        By entrar = By.XPath("//input[@type='submit']");
        By mensagemLoginIncorreto = By.XPath("//div[@class='alert alert-danger']");
        #endregion

        #region Actions
        public void PreencherUsuario(string usuario)
        {
            SendKeysJavaScript(nomeDoUtilizador, usuario);
        }

        public void PreencherSenha(string senha)
        {
            SendKeysJavaScript(palavraChave, senha);
        }

        public void ClicarEmEntrar()
        {
            ClickJavaScript(entrar);
        }

        public string RetornaMensagemDeErroNoLogin()
        {
            return GetText(mensagemLoginIncorreto);
        }
        #endregion
    }
}
