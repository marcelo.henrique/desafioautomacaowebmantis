﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MainPage : PageBase
    {
        #region Mapping
        By minhaVisao = By.XPath("//i[@class='menu-icon fa fa-dashboard']");
        By verTarefas = By.XPath("//i[@class='menu-icon fa fa-list-alt']");
        By criarTarefa = By.XPath("//i[@class='menu-icon fa fa-edit']");
        By registroDeMudancas = By.XPath("//i[@class='menu-icon fa fa-retweet']");
        By planejamento = By.XPath("//i[@class='menu-icon fa fa-road']");
        By resumo = By.XPath("//i[@class='menu-icon fa fa-bar-chart-o']");
        By gerenciar = By.XPath("//i[@class='menu-icon fa fa-gears']");
        By usernameLoginLink = By.LinkText("administrator");
        #endregion

        #region Actions
        public string RetornaUsernameDasInformacoesDeLogin()
        {
            return GetText(usernameLoginLink);
        }
        public void ClicarEmMinhaVisao()
        {
            Click(minhaVisao);
        }
        public void ClicarEmVerTarefas()
        {
            Click(verTarefas);
        }
        public void ClicarEmCriarTarefa()
        {
            Click(criarTarefa);
        }
        public void ClicarEmRegistroDeMudancas()
        {
            Click(registroDeMudancas);
        }
        public void ClicarEmPlanejamento()
        {
            Click(planejamento);
        }
        public void ClicarEmResumo()
        {
            Click(registroDeMudancas);
        }
        public void ClicarEmGerenciar()
        {
            Click(gerenciar);
        }
        #endregion
    }
}
