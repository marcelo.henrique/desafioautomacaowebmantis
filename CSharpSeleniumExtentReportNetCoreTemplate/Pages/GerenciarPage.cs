using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class GerenciarPage : PageBase
    {
        #region Mapping
        By gerenciarUsuariosTab = By.XPath("//a[text()='Gerenciar Usuários']");
        By gerenciarMarcadoresTab = By.XPath("//a[text()='Gerenciar Marcadores']");
        By gerenciarCamposPersonalizadosTab = By.XPath("//a[text()='Gerenciar Campos Personalizados']");
        By gerenciarProjetosTab = By.XPath("//a[text()='Gerenciar Projetos']");
        #endregion

        #region Actions
        public void ClicarEmGerenciarUsuarios()
        {
            Click(gerenciarUsuariosTab);
        }

        public void ClicarEmGerenciarMarcadores()
        {
            Click(gerenciarMarcadoresTab);
        }

        public void ClicarEmGerenciarCamposPersonalizados()
        {
            Click(gerenciarCamposPersonalizadosTab);
        }

        public void ClicarEmGerenciarProjetos()
        {
            Click(gerenciarProjetosTab);
        }

        #endregion
    }
}
