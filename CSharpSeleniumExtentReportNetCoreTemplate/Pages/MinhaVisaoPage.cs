using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Pages
{
    public class MinhaVisaoPage : PageBase
    {
        #region Mapping

        #endregion

        #region Actions
        public string RetornaTarefasAtribuidasAMim(string numeroTarefa)
        {
            return GetText(By.XPath("//div[@id='assigned']//a[text()='" + numeroTarefa + "']"));
        }
        
        public String RetornaTarefaNaoAtribuidaANenhumUsuario(string numeroTarefa)
        {
            return GetText(By.XPath("//div[@id='unassigned']//a[text()='" + numeroTarefa + "']"));
        }

        public string RetornaTarefaMonitorada(string numeroTarefa)
        {
            return GetText(By.XPath("//div[@id='monitored']//a[text()='"+numeroTarefa+"']"));
        }

        public string RetornaNumeroTarefaModificada(string numeroTarefa)
        {
            return GetText(By.XPath("//div[@id='recent_mod']//a[text()='" + numeroTarefa + "']"));
        }

        #endregion
    }
}
