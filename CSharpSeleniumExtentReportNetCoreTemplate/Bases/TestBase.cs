﻿using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using NUnit.Framework;
using Castle.DynamicProxy;
using System.Collections.Generic;

using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Bases
{
    public class TestBase
    {
        [SetUp]
        public void Setup()
        {
            ExtentReportHelpers.AddTest();
            DriverFactory.CreateInstance();
            DriverFactory.INSTANCE.Navigate().GoToUrl(BuilderJson.ReturnParameterAppSettings("DEFAULT_APPLICATION_URL"));
            LimparDadosBancoDBSteps.LimparDadosBD();

            //Criar massa de dados para testes
            //Criar projeto
            CriarProjetoDBSteps.CriarProjeto();

            //Criar campo personalizado
            CriarCampoPersonalizadoDBSteps.CriarCampoPersonalizado();

            //Criar marcador
            CriarMarcadorDBSteps.CriarMarcador();

            //Criar categoria
            CriarCategoriaDBSteps.CriarCategoria();

            //Criar tarefa e anotação
            CriarTarefaDBSteps.CriarTarefa();
            CriarAnotacaoDBSteps.CriarAnotacao();

            //Criar usuario
            CriarUsuarioDBSteps.CriarUsuario();

            
        }

        [TearDown]
        public void TearDown()
        {
            ExtentReportHelpers.AddTestResult();
            DriverFactory.QuitInstace();
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            ExtentReportHelpers.CreateReport();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            ExtentReportHelpers.GenerateReport();
        }
    }
}
