using System;
using System.Collections.Generic;
using System.Data;
//using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.IO;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Helpers
{
    public class DataDrivenHelpers
    {
       public static List<TestCaseData> ReturnDadosTarefa_CSV
        {
            get
            {
                var testCases = new List<TestCaseData>();
        
                using (var fs = File.OpenRead(GeneralHelpers.GetProjectPath() + @"\DataDriven\tarefa.csv"))
                using (var sr = new StreamReader(fs))
                {
                    string headerLine = sr.ReadLine();

                    string line = string.Empty;
                    while (line != null)
                    {
                        line = sr.ReadLine();

                        if (line != null)
                        {
                            string[] split = line.Split(new char[] { ',' },
                                StringSplitOptions.None);
        
                            string param1 = Convert.ToString(split[0]); //categoria
                            string param2 = Convert.ToString(split[1]); //frequencia
                            string param3 = Convert.ToString(split[2]); //gravidade
                            string param4 = Convert.ToString(split[3]); //prioridade
                            string param5 = Convert.ToString(split[4]); //atribuirA
                            string param6 = Convert.ToString(split[5]); //resumo
                            string param7 = Convert.ToString(split[6]); //descricao
                            string param8 = Convert.ToString(split[7]); //passosParaReproduzir
                            string param9 = Convert.ToString(split[8]); //informacoesAdicionais
        
                            var testCase = new TestCaseData(param1, param2, param3, param4, param5, param6, param7, param8, param9);
                            testCases.Add(testCase);
                        }
                    }
                }
        
                return testCases;
            }
        }//end method
    }
}
