using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class GerenciarMarcadoresTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        GerenciarPage gerenciarPage;
        GerenciarMarcadoresPage gerenciarMarcadoresPage;
        #endregion

        [Test]
        public void CriarMarcadorComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeMarcador = "MarcadorTesteDesafioBase2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarMarcadores();
            gerenciarMarcadoresPage.PreencherCampoNome(nomeMarcador);
            gerenciarMarcadoresPage.ClicarEmCriarMarcador();

            Assert.AreEqual(nomeMarcador, gerenciarMarcadoresPage.RetornaNomeMarcador(nomeMarcador));
        }

        [Test]
        public void AlterarMarcadorComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeMarcadorAlterado = "MarcadorTesteDesafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarMarcadores();
            gerenciarMarcadoresPage.ClicarNoMarcador();
            gerenciarMarcadoresPage.ClicarEmAtualizarMarcador();
            gerenciarMarcadoresPage.PreencherCampoNome(nomeMarcadorAlterado);
            gerenciarMarcadoresPage.ClicarEmAtualizarMarcador();

            Assert.AreEqual(nomeMarcadorAlterado, gerenciarMarcadoresPage.RetornaNomeMarcadorAlterado(nomeMarcadorAlterado));
        }

        [Test]
        public void ApagarMarcadorComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarMarcadoresPage = new GerenciarMarcadoresPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarMarcadores();
            gerenciarMarcadoresPage.ClicarNoMarcador();
            gerenciarMarcadoresPage.ClicarEmApagarMarcador();
            gerenciarMarcadoresPage.ClicarEmApagarMarcador();

            Assert.AreEqual("", gerenciarMarcadoresPage.RetornaInformacoesMarcador());
        }


    }
}
