using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CriarTarefaTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        CriarTarefaPage criarTarefaPage;
        VerDetalhesDaTarefaPage verDetalhesDaTarefaPage;
        MinhaVisaoPage minhaVisaoPage;

        #endregion

        [Test]
        public void CriarTarefaComSucessoPreenchendoTodosOsCampos()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            criarTarefaPage = new CriarTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string categoria = "[Todos os Projetos] General";
            string frequencia = "sempre";
            string gravidade = "pequeno";
            string prioridade = "normal";
            string atribuirA = "administrator";
            string resumo = "CriarTarefaComSucessoPreenchendoTodosOsCampos";
            string descricao = "CriarTarefaComSucessoPreenchendoTodosOsCampos";
            string passosParaReproduzir = "Teste";
            string informacoesAdicionais = "Teste";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmCriarTarefa();
            criarTarefaPage.SelecionarCategoria(categoria);
            criarTarefaPage.SelecionarFrequencia(frequencia);
            criarTarefaPage.SelecionarGravidade(gravidade);
            criarTarefaPage.SelecionarPrioridade(prioridade);
            criarTarefaPage.SelecionarAtribuirA(atribuirA);
            criarTarefaPage.PreencherResumo(resumo);
            criarTarefaPage.PreencherDescricao(descricao);
            criarTarefaPage.PreencherPassosParaReproduzir(passosParaReproduzir);
            criarTarefaPage.PreencherInformacoesAdicionais(informacoesAdicionais);
            criarTarefaPage.ClicarEmCriarNovaTarefa();


            Assert.AreEqual("Operação realizada com sucesso.", criarTarefaPage.PegarTextoOperacaoRealizadaComSucesso());
        }

        [Test]
        public void CriarTarefaSemAtribuirUsuarioEValidarEmVerDetalhesDaTarefa()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            criarTarefaPage = new CriarTarefaPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string categoria = "[Todos os Projetos] General";
            string frequencia = "sempre";
            string gravidade = "pequeno";
            string prioridade = "normal";
            string resumo = "CriarTarefaSemAtribuirUsuarioEValidarEmVerDetalhesDaTarefa";
            string descricao = "CriarTarefaSemAtribuirUsuarioEValidarEmVerDetalhesDaTarefa";
            string passosParaReproduzir = "Teste";
            string informacoesAdicionais = "Teste";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmCriarTarefa();
            criarTarefaPage.SelecionarCategoria(categoria);
            criarTarefaPage.SelecionarFrequencia(frequencia);
            criarTarefaPage.SelecionarGravidade(gravidade);
            criarTarefaPage.SelecionarPrioridade(prioridade);
            criarTarefaPage.PreencherResumo(resumo);
            criarTarefaPage.PreencherDescricao(descricao);
            criarTarefaPage.PreencherPassosParaReproduzir(passosParaReproduzir);
            criarTarefaPage.PreencherInformacoesAdicionais(informacoesAdicionais);
            criarTarefaPage.ClicarEmCriarNovaTarefa();

            Assert.AreEqual("", verDetalhesDaTarefaPage.RetornaTextoAtribuidoA());
        }

        [Test]
        public void CriarTarefaAtribuirAMimEValidarEmMinhaVisao()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            criarTarefaPage = new CriarTarefaPage();
            minhaVisaoPage = new MinhaVisaoPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string categoria = "[Todos os Projetos] General";
            string frequencia = "sempre";
            string gravidade = "pequeno";
            string prioridade = "normal";
            string atribuirA = "administrator";
            string resumo = "CriarTarefaAtribuirAMimEValidarEmMinhaVisao";
            string descricao = "CriarTarefaAtribuirAMimEValidarEmMinhaVisao";
            string passosParaReproduzir = "Teste";
            string informacoesAdicionais = "Teste";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmCriarTarefa();
            criarTarefaPage.SelecionarCategoria(categoria);
            criarTarefaPage.SelecionarFrequencia(frequencia);
            criarTarefaPage.SelecionarGravidade(gravidade);
            criarTarefaPage.SelecionarPrioridade(prioridade);
            criarTarefaPage.SelecionarAtribuirA(atribuirA);
            criarTarefaPage.PreencherResumo(resumo);
            criarTarefaPage.PreencherDescricao(descricao);
            criarTarefaPage.PreencherPassosParaReproduzir(passosParaReproduzir);
            criarTarefaPage.PreencherInformacoesAdicionais(informacoesAdicionais);
            criarTarefaPage.ClicarEmCriarNovaTarefa();
            mainPage.ClicarEmMinhaVisao();

            Assert.AreEqual(descricao, minhaVisaoPage.RetornaTarefasAtribuidasAMim(descricao));
        }

        [Test]
        public void CriarTarefaSemAtribuirUsuarioEValidarEmMinhaVisao()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            criarTarefaPage = new CriarTarefaPage();
            minhaVisaoPage = new MinhaVisaoPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string categoria = "[Todos os Projetos] General";
            string frequencia = "sempre";
            string gravidade = "pequeno";
            string prioridade = "normal";
            string resumo = "CriarTarefaSemAtribuirUsuarioEValidarEmMinhaVisao";
            string descricao = "CriarTarefaSemAtribuirUsuarioEValidarEmMinhaVisao";
            string passosParaReproduzir = "Teste";
            string informacoesAdicionais = "Teste";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmCriarTarefa();
            criarTarefaPage.SelecionarCategoria(categoria);
            criarTarefaPage.SelecionarFrequencia(frequencia);
            criarTarefaPage.SelecionarGravidade(gravidade);
            criarTarefaPage.SelecionarPrioridade(prioridade);
            criarTarefaPage.PreencherResumo(resumo);
            criarTarefaPage.PreencherDescricao(descricao);
            criarTarefaPage.PreencherPassosParaReproduzir(passosParaReproduzir);
            criarTarefaPage.PreencherInformacoesAdicionais(informacoesAdicionais);
            criarTarefaPage.ClicarEmCriarNovaTarefa();
            mainPage.ClicarEmMinhaVisao();

            Assert.AreEqual(descricao, minhaVisaoPage.RetornaTarefaNaoAtribuidaANenhumUsuario(descricao));
        }


    }


}
