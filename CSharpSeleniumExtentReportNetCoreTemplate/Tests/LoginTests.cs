﻿using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class LoginTests : TestBase
    {

        #region Pages and Flows Objects
        LoginPage loginPage;
        MainPage mainPage;
        #endregion

        [Test]
        public void RealizarLoginComSucesso()
        {
            loginPage = new LoginPage();
            mainPage = new MainPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginPage.PreencherUsuario(usuario);
            loginPage.ClicarEmEntrar();
            loginPage.PreencherSenha(senha);
            loginPage.ClicarEmEntrar();

            Assert.AreEqual(usuario, mainPage.RetornaUsernameDasInformacoesDeLogin());
        }

        [Test]
         public void ValidarLoginComUsuarioIncorreto()
        {

            loginPage = new LoginPage();
            mainPage = new MainPage();

            #region Parameters
            string usuario = "testeUsuarioIncorreto";
            string senhaIncorreta = "administrator";
            string mensagemDeErroNoUsuario = "Sua conta pode estar desativada ou bloqueada ou o nome de usuário e a senha que você digitou não estão corretos.";
            #endregion

            loginPage.PreencherUsuario(usuario);
            loginPage.ClicarEmEntrar();
            loginPage.PreencherSenha(senhaIncorreta);
            loginPage.ClicarEmEntrar();

            Assert.AreEqual(mensagemDeErroNoUsuario, loginPage.RetornaMensagemDeErroNoLogin());
        }

        [Test]
        public void ValidarLoginComSenhaIncorreta()
        {

            loginPage = new LoginPage();
            mainPage = new MainPage();

            #region Parameters
            string usuario = "administrator";
            string senhaIncorreta = "testeSenhaIncorreta";
            string mensagemDeErroNaSenha = "Sua conta pode estar desativada ou bloqueada ou o nome de usuário e a senha que você digitou não estão corretos.";
            #endregion

            loginPage.PreencherUsuario(usuario);
            loginPage.ClicarEmEntrar();
            loginPage.PreencherSenha(senhaIncorreta);
            loginPage.ClicarEmEntrar();

            Assert.AreEqual(mensagemDeErroNaSenha, loginPage.RetornaMensagemDeErroNoLogin());
        }

    }
}
