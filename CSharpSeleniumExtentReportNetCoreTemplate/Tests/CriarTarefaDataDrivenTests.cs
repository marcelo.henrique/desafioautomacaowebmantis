using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;
using Microsoft.VisualStudio;


namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class CriarTarefaDataDrivenTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        CriarTarefaPage criarTarefaPage;
        #endregion

        [TestCaseSource(typeof(DataDrivenHelpers), "ReturnDadosTarefa_CSV")]
        public void CriarTarefaComSucesso(string categoria, string frequencia, string gravidade, string prioridade, 
        string atribuirA, string resumo, string descricao, string passosParaReproduzir, string informacoesAdicionais)
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            criarTarefaPage = new CriarTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmCriarTarefa();
            criarTarefaPage.ClicarEmSelecionarProjeto();
            criarTarefaPage.SelecionarCategoria(categoria);
            criarTarefaPage.SelecionarFrequencia(frequencia);
            criarTarefaPage.SelecionarGravidade(gravidade);
            criarTarefaPage.SelecionarPrioridade(prioridade);
            criarTarefaPage.SelecionarAtribuirA(atribuirA);
            criarTarefaPage.PreencherResumo(resumo);
            criarTarefaPage.PreencherDescricao(descricao);
            criarTarefaPage.PreencherPassosParaReproduzir(passosParaReproduzir);
            criarTarefaPage.PreencherInformacoesAdicionais(informacoesAdicionais);
            criarTarefaPage.ClicarEmCriarNovaTarefa();


            Assert.AreEqual("Operação realizada com sucesso.", criarTarefaPage.PegarTextoOperacaoRealizadaComSucesso());
        }
    }
}