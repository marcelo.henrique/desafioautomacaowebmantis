using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class GerenciarProjetosTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        GerenciarPage gerenciarPage;
        GerenciarProjetosPage gerenciarProjetosPage;
        #endregion

        [Test]
        public void CriarProjetoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeDoProjeto = "ProjetoTesteDesafioBase2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmCriarNovoProjeto();
            gerenciarProjetosPage.PreencherCampoNomeDoProjeto(nomeDoProjeto);
            gerenciarProjetosPage.ClicarEmAdicionarProjeto();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarProjetosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ApagarProjetoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarNoProjeto();
            gerenciarProjetosPage.ClicarEmApagarProjeto();
            gerenciarProjetosPage.ClicarEmApagarProjeto();

            Assert.AreEqual("", gerenciarProjetosPage.RetornaInformacoesDoProjeto());
        }

        [Test]
        public void AlterarProjetoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeProjetoAlterado = "Projeto Desafio";
            string estado = "obsoleto";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarNoProjeto();
            gerenciarProjetosPage.PreencherCampoNomeDoProjeto(nomeProjetoAlterado);
            gerenciarProjetosPage.SelecionarEstadoDoProjeto(estado);
            gerenciarProjetosPage.ClicarEmAtualizarProjeto();

            Assert.AreEqual(nomeProjetoAlterado, gerenciarProjetosPage.RetornaNomeDoProjeto(nomeProjetoAlterado));
        }

        [Test]
        public void CriarCategoriaComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCategoria = "Categoria Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.PreencherNomeCategoria(nomeCategoria);
            gerenciarProjetosPage.ClicarEmAdicionarCategoria();

            Assert.AreEqual(nomeCategoria, gerenciarProjetosPage.RetornaNomeDaCategoria(nomeCategoria));
        }

        [Test]
        public void ValidarBotaoAdicionarEEditarCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCategoria = "Categoria Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.PreencherNomeCategoria(nomeCategoria);
            gerenciarProjetosPage.ClicarEmAdicionarEEditarCategoria();
            gerenciarProjetosPage.ClicarEmAtualizarCategoria();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarProjetosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ValidarObrigatoriedadeCampoNomeCategoriaClicandoEmAdicionarCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmAdicionarCategoria();

            Assert.AreEqual("APPLICATION ERROR #11", gerenciarProjetosPage.RetornaMensagemDeErro());
        }

        [Test]
        public void ValidarObrigatoriedadeCampoNomeCategoriaClicandoEmAdicionarEEditarCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmAdicionarEEditarCategoria();

            Assert.AreEqual("APPLICATION ERROR #11", gerenciarProjetosPage.RetornaMensagemDeErro());
        }

        [Test]
        public void AlterarCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCategoriaAlterado = "Categoria Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmAlterarCategoria();
            gerenciarProjetosPage.PreencherCategoria(nomeCategoriaAlterado);
            gerenciarProjetosPage.ClicarEmAtualizarCategoria();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarProjetosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ValidarAlteraçãoDeCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCategoriaAlterado = "Categoria Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmAlterarCategoria();
            gerenciarProjetosPage.PreencherCategoria(nomeCategoriaAlterado);
            gerenciarProjetosPage.ClicarEmAtualizarCategoria();

            Assert.AreEqual(nomeCategoriaAlterado, gerenciarProjetosPage.RetornaNomeDaCategoria(nomeCategoriaAlterado));
        }

        [Test]
        public void ApagarCategoria()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarProjetosPage = new GerenciarProjetosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarProjetos();
            gerenciarProjetosPage.ClicarEmApagarCategoria();
            gerenciarProjetosPage.ClicarEmConfirmarApagarCategoria();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarProjetosPage.RetornaMensagemDeSucesso());
        }
    }
}
