using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class GerenciarUsuariosTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        GerenciarPage gerenciarPage;
        GerenciarUsuariosPage gerenciarUsuariosPage;
        #endregion

        [Test]
        public void ApagarUsuarioComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeUsuario = "UsuarioDesafioBase2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarNoUsuario(nomeUsuario);
            gerenciarUsuariosPage.ClicarEmApagar();
            gerenciarUsuariosPage.ClicarEmApagarConta();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarUsuariosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void CadastrarUsuarioComEmailInvalido()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeUsuario = "UsuarioDesafioBase2";
            string nomeVerdadeiro = "UsuarioTeste";
            string email = "emailinvalido.com";
            string nivelAcesso = "atualizador";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarEmCriarNovaConta();
            gerenciarUsuariosPage.PreencherCampoNomeUsuario(nomeUsuario);
            gerenciarUsuariosPage.PreencherCampoNomeVerdadeiro(nomeVerdadeiro);
            gerenciarUsuariosPage.PreencherCampoEmail(email);
            gerenciarUsuariosPage.SelecionarNivelDeAcessoCombobox(nivelAcesso);
            gerenciarUsuariosPage.ClicarEmCriarUsuario();

            Assert.AreEqual("APPLICATION ERROR #800", gerenciarUsuariosPage.RetornaMensagemDeErro());
        }

        [Test]
        public void CadastrarUsuarioSemInformarNomeDeUsuario()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarEmCriarNovaConta();
            gerenciarUsuariosPage.ClicarEmCriarUsuario();

            Assert.AreEqual("APPLICATION ERROR #805", gerenciarUsuariosPage.RetornaMensagemDeErro());
        }

        [Test]
        public void DesativarUsuario()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeUsuario = "UsuarioDesafioBase2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarNoUsuario(nomeUsuario);
            gerenciarUsuariosPage.ClicarNoCheckboxHabilitado();
            gerenciarUsuariosPage.ClicarEmAtualizarUsuario();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarUsuariosPage.RetornaMensagemDeSucesso());
        }

         [Test]
        public void AlterarNivelAcessoUsuario()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeUsuario = "UsuarioDesafioBase2";
            string nivelAcesso = "desenvolvedor";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarNoUsuario(nomeUsuario);
            gerenciarUsuariosPage.SelecionarNovoNivelDeAcessoCombobox(nivelAcesso);
            gerenciarUsuariosPage.ClicarEmAtualizarUsuario();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarUsuariosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void DesativarUsuarioEValidarEmGerenciarContas()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarUsuariosPage = new GerenciarUsuariosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeUsuario = "UsuarioDesafioBase2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarNoUsuario(nomeUsuario);
            gerenciarUsuariosPage.ClicarNoCheckboxHabilitado();
            gerenciarUsuariosPage.ClicarEmAtualizarUsuario();
            gerenciarPage.ClicarEmGerenciarUsuarios();
            gerenciarUsuariosPage.ClicarNoCheckboxMostrarDesativados();
            gerenciarUsuariosPage.ClicarEmAplicarFiltro();

            Assert.AreEqual(nomeUsuario, gerenciarUsuariosPage.RetornaUsuario(nomeUsuario));
        }
    }
}
