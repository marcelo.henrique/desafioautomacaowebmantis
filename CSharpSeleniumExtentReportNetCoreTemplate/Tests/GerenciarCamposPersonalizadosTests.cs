using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class GerenciarCamposPersonalizadosTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        GerenciarPage gerenciarPage;
        GerenciarCamposPersonalizadosPage gerenciarCamposPersonalizadosPage;
        #endregion

        [Test]
        public void CriarCampoPersonalizadoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCampoPersonalizado = "Campo_Personalizado_Desafio_Base2";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.PreencherCampoNome(nomeCampoPersonalizado);
            gerenciarCamposPersonalizadosPage.ClicarEmNovoCampoPersonalizado();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ValidarObrigatoriedadeDoCampoNomeCampoPersonalizado()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.ClicarEmNovoCampoPersonalizado();

            Assert.AreEqual("APPLICATION ERROR #11", gerenciarCamposPersonalizadosPage.RetornaMensagemDeErro());
        }

        [Test]
        public void AlterarCampoPersonalizado()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCampoPersonalizadoAlterado = "Campo personalizado Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.ClicarNoCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.PreencherCampoNome(nomeCampoPersonalizadoAlterado);
            gerenciarCamposPersonalizadosPage.ClicarEmAtualizarCampoPersonalizado();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ApagarCampoPersonalizadoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.ClicarNoCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.ClicarEmApagarCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.ClicarEmApagarCampo();

            Assert.AreEqual("", gerenciarCamposPersonalizadosPage.RetornaInformacoesCampoPersonalizado());
        }

        [Test]
        public void ValidarApagarCampoPersonalizado()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.ClicarNoCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.ClicarEmApagarCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.ClicarEmApagarCampo();

            Assert.AreEqual("Operação realizada com sucesso.", gerenciarCamposPersonalizadosPage.RetornaMensagemDeSucesso());
        }

        [Test]
        public void ValidarAlterarCampoPersonalizado()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            gerenciarPage = new GerenciarPage();
            gerenciarCamposPersonalizadosPage = new GerenciarCamposPersonalizadosPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string nomeCampoPersonalizadoAlterado = "Campo personalizado Desafio";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmGerenciar();
            gerenciarPage.ClicarEmGerenciarCamposPersonalizados();
            gerenciarCamposPersonalizadosPage.ClicarNoCampoPersonalizado();
            gerenciarCamposPersonalizadosPage.PreencherCampoNome(nomeCampoPersonalizadoAlterado);
            gerenciarCamposPersonalizadosPage.ClicarEmAtualizarCampoPersonalizado();

            Assert.AreEqual(nomeCampoPersonalizadoAlterado, gerenciarCamposPersonalizadosPage.RetornaNomeCampoPersonalizado(nomeCampoPersonalizadoAlterado));
        }

    }
}
