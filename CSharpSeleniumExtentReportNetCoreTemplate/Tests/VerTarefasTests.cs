using CSharpSeleniumExtentReportNetCoreTemplate.Bases;
using CSharpSeleniumExtentReportNetCoreTemplate.DataBaseSteps;
using CSharpSeleniumExtentReportNetCoreTemplate.Helpers;
using CSharpSeleniumExtentReportNetCoreTemplate.Pages;
using CSharpSeleniumExtentReportNetCoreTemplate.Flows;
using NUnit.Framework;
using System.Collections;

namespace CSharpSeleniumExtentReportNetCoreTemplate.Tests
{
    [TestFixture]
    public class VerTaresfaTests : TestBase
    {

        #region Pages and Flows Objects
        LoginFlows loginFlows;
        MainPage mainPage;
        VerTarefasPage verTarefasPage;
        VerDetalhesDaTarefaPage verDetalhesDaTarefaPage;
        MinhaVisaoPage minhaVisaoPage;

        #endregion

        [Test]
        public void EnviarUmLembreteComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmEnviarUmLembrete();
            verDetalhesDaTarefaPage.SelecionarUsuarioLembrete();
            verDetalhesDaTarefaPage.ClicarEmEnviar();


            Assert.AreEqual("Operação realizada com sucesso.", verDetalhesDaTarefaPage.RetornaTextoOperacaoRealizadaComSucesso());
        }

        [Test]
        public void EnviarUmLembreteSemSelecionarUsuario()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmEnviarUmLembrete();
            verDetalhesDaTarefaPage.ClicarEmEnviar();


            Assert.AreEqual("APPLICATION ERROR #200", verDetalhesDaTarefaPage.RetornaTextoErroNaAplicacao());
        }

        [Test]
        public void ValidarEnvioDeLembrete()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmEnviarUmLembrete();
            verDetalhesDaTarefaPage.SelecionarUsuarioLembrete();
            verDetalhesDaTarefaPage.ClicarEmEnviar();


            Assert.IsTrue(verDetalhesDaTarefaPage.RetornaAtividadesInformacao().Contains("Lembrete mandado para: administrator"));
        }

        [Test]
        public void ValidarRedirecionamentoBotaoIrParaAsAnotacoes()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmIrParaAsAnotacoes();

            Assert.AreEqual("Atividades", verDetalhesDaTarefaPage.RetornaTextoAtividades());
        }

        [Test]
        public void AdicionarAnotacaoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string anotacao = "AdicionarAnotacaoComSucesso";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmIrParaAsAnotacoes();
            verDetalhesDaTarefaPage.PreencherCampoAnotacao(anotacao);
            verDetalhesDaTarefaPage.ClicarEmAdicionarAnotacao();
            verDetalhesDaTarefaPage.ClicarEmIrParaAsAnotacoes();

            Assert.AreEqual(anotacao, verDetalhesDaTarefaPage.RetornaDescriçãoAtividade());
        }


        [Test]
        public void AdicionarAnotacaoSemPreencherCampo()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmIrParaAsAnotacoes();
            verDetalhesDaTarefaPage.ClicarEmAdicionarAnotacao();

            Assert.AreEqual("APPLICATION ERROR #11", verDetalhesDaTarefaPage.RetornaTextoErroNaAplicacao());
        }

        [Test]
        public void ApagarAnotacaoComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmApagarAtividade();
            verDetalhesDaTarefaPage.ClicarEmApagarAnotacao();
            verDetalhesDaTarefaPage.RetornaTextoNaoHaAnotacao();

            Assert.AreEqual("Não há anotações anexadas a esta tarefa.", verDetalhesDaTarefaPage.RetornaTextoNaoHaAnotacao());

        }

        [Test]
        public void ResolverTarefaComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string status = "resolvido";
            string resolucao = "corrigido";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.SelecionarAlterarStatusTerafaCombobox(status);
            verDetalhesDaTarefaPage.ClicarEmAlterarStatusTarefaButton();
            verDetalhesDaTarefaPage.SelecionarResolucaoTarefaCombobox(resolucao);
            verDetalhesDaTarefaPage.ClicarEmResolverTarefaButton();

            Assert.AreEqual(status, verDetalhesDaTarefaPage.RetornaTextoEstadoTarefa());
        }

        [Test]
        public void MarcarTarefaComoPegajoso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmMarcarComoPegajosoButton();

            Assert.IsTrue(verDetalhesDaTarefaPage.RetornaTextoHistoricoAtividade().Contains("Tarefa \"Pegajosa\""));
        }

        [Test]
        public void DesmarcarTarefaComoPegajoso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmMarcarComoPegajosoButton();
            verDetalhesDaTarefaPage.ClicarEmDesmarcarComoPegajosoButton();

            Assert.IsTrue(verDetalhesDaTarefaPage.RetornaTextoHistoricoAtividade().Contains("Tarefa \"Pegajosa\""));
        }

        [Test]
        public void MonitorarTarefa()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmMonitorarTarefaButton();

            Assert.AreEqual(usuario, verDetalhesDaTarefaPage.RetornaTextoUsuarioMonitorando());
        }

        [Test]
        public void MonitorarTarefaValidarEmMinhaVisao()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();
            minhaVisaoPage = new MinhaVisaoPage();


            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmMonitorarTarefaButton();
            //Pegar numero da tarefa modificada
            string numeroTarefaMonitorada = verDetalhesDaTarefaPage.RetornaNumeroTarefaModificada();
            mainPage.ClicarEmMinhaVisao();

            Assert.AreEqual(numeroTarefaMonitorada, minhaVisaoPage.RetornaTarefaMonitorada(numeroTarefaMonitorada));
        }

        [Test]
        public void ValidarBotaoIrParaHistorico()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmIrParaOHistorico();

            Assert.IsTrue(verDetalhesDaTarefaPage.RetornaTextoHistoricoTarefa().Contains("Histórico da Tarefa"));
        }

        [Test]
        public void FecharTarefaComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmFechar();
            verDetalhesDaTarefaPage.ClicarEmFecharTarefa();

            Assert.AreEqual("fechado", verDetalhesDaTarefaPage.RetornaTextoEstadoTarefa());
        }

        [Test]
        public void EditarTarefaComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string estado = "admitido";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmAtualizarTarefa();
            verDetalhesDaTarefaPage.SelecionarEstadoTarefaCombobox(estado);
            verDetalhesDaTarefaPage.ClicarEmSalvarTarefa();

            Assert.AreEqual(estado, verDetalhesDaTarefaPage.RetornaTextoEstadoTarefa());
        }

         [Test]
        public void EditarTarefaValidarEmMinhaVisao()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();
            minhaVisaoPage = new MinhaVisaoPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string estado = "admitido";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmAtualizarTarefa();
            verDetalhesDaTarefaPage.SelecionarEstadoTarefaCombobox(estado);
            verDetalhesDaTarefaPage.ClicarEmSalvarTarefa();
            //Pegar numero tarefa editada
            string numeroTarefa = verDetalhesDaTarefaPage.RetornaNumeroTarefa();
            mainPage.ClicarEmMinhaVisao();

            Assert.AreEqual(numeroTarefa, minhaVisaoPage.RetornaNumeroTarefaModificada(numeroTarefa));
        }

        [Test]
        public void ApagarTarefaComSucesso()
        {
            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();
            minhaVisaoPage = new MinhaVisaoPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarEmRedefinir();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmApagar();
            verDetalhesDaTarefaPage.ClicarEmApagarTarefas();

            Assert.AreEqual("", verTarefasPage.RetornaInformacoesDaTarefaNaTabela());
        }

        [Test]
        public void ReabrirTarefaComSucesso()
        {
            CriarTarefaDBSteps.CriarTarefaFechada();

            loginFlows = new LoginFlows();
            mainPage = new MainPage();
            verTarefasPage = new VerTarefasPage();
            verDetalhesDaTarefaPage = new VerDetalhesDaTarefaPage();

            #region Parameters
            string usuario = "administrator";
            string senha = "administrator";
            string estado = "fechado";
            #endregion

            loginFlows.EfetuarLogin(usuario, senha);
            mainPage.ClicarEmVerTarefas();
            verTarefasPage.ClicarNoFiltroEstadoDaTarefa();
            verTarefasPage.SelecionarEstadoDaTarefa(estado);
            verTarefasPage.ClicarEmAplicarFiltro();
            verTarefasPage.ClicarNaTarefa();
            verDetalhesDaTarefaPage.ClicarEmReabrirTarefa();
            verDetalhesDaTarefaPage.ClicarEmSolicitarRetorno();

            Assert.AreEqual("retorno", verDetalhesDaTarefaPage.RetornaTextoEstadoTarefa());
        }

    }
}