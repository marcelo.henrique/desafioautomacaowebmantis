## Automação

- Arquitetura Projeto
	- Linguagem		- [CSharp](https://docs.microsoft.com/pt-br/dotnet/csharp/ "CSharp")
	- Framework de desenvolvimento - [.Net Core](https://dotnet.microsoft.com/download/dotnet-core/3.1)
	- Execução dos testes - [SeleniumGrid](https://github.com/SeleniumHQ/selenium/wiki/Grid2 "SeleniumGrid")
	- Orquestrador de testes - [NUnit 3.11](https://github.com/nunit/nunit "NUnit 3.11")
	- Relatório de testes automatizados - [ExtentReports.Core 1.0.3](https://www.nuget.org/packages/ExtentReports.Core/)
	- Framework interação com elementos web - [Selenium.WebDriver 3.141](https://www.seleniumhq.org/download/ "Selenium.WebDriver") 

## .Net Core e Visual Code
Estamos adotando o uso do Visual Code para o desenvolvimento de projetos CSharp que estão em framework .Net Core, para isso recomendamos a configuração da ferramenta e seus recursos conforme artigo explicativo: [clique aqui](https://medium.com/@saymowan/configurando-seu-vscode-para-desenvolver-projetos-de-testes-automatizados-netcore-nunit-476e73aa7b01).


## Arquitetura

**Premissas de uma boa arquitetura de automação de testes:**
*  Facilitar o desenvolvimento dos testes automatizados (reuso).
*  Facilitar a manutenção dos testes (refatoração).
*  Tornar o fluxo do teste o mais legível possível (fácil entendimento do que está sendo testado).

**Arquitetura padrão Base2**

Para facilitar o entendimento da arquitetura do projeto de testes automatizados, foi criado um fluxograma baseado nas features principais que envolvam a arquitetura conforme imagem abaixo:

![alt text](https://i.imgur.com/AXY9ukW.png)



# Padrões de escrita de código

O padrão adotado para escrita é o “CamelCase” onde uma palavra é separada da outra através de letras maiúsculas. Este padrão é adotado para o nome de pastas, classes, métodos, variáveis e arquivos em geral exceto constantes. Constantes devem ser escritas com todas suas letras em maiúsculo separando as palavras com “_”.

Ex: `PreencherUsuario(), nomeUsuario, LoginPage etc.`

**Padrões por tipo de componente**

* Pastas: começam sempre com letra maiúscula. Ex: `Pages, DataBaseSteps, Queries, Bases`
* Classes: começam sempre com letra maiúscula. Ex: `LoginPage, LoginTests`
* Arquivos: começam sempre com letra maiúscula. Ex: `DataDrivenFile.csv`
* Métodos: começam sempre com letra maiúscula. Ex: `VerificarElementoXPTO()`
* Variáveis: começam sempre com letra minúscula. Ex: `botaoXPTO`
* Objetos: começam sempre com letra minúscula. Ex: `loginPage`


**Padrão de siglas e palavras com uma letra**

No caso de siglas, manter o padrão da primeira letra, de acordo com o padrão do tipo que será nomeado, ex:

```
cpfField (variável)
PreencherCPF() (método)
```

No caso de palavras com uma letra, as duas devem ser escritas juntas de acordo com o padrão do tipo que será nomeado, ex:`retornaSeValorEOEsperado()`



**Padrões de escrita: Classes e Arquivos**

Nomes de classes e arquivos devem terminar com o tipo de conteúdo que representam, em inglês, ex:

```
LoginPage (classe de PageObjects)
LoginTests (classe de testes)
LoginTestData.csv (planilha de dados)
```

OBS: Atenção ao plural e singular! Se a classe contém um conjunto do tipo que representa, esta deve ser escrita no plural, caso seja uma entidade, deve ser escrita no singular.


**Padrões de escrita: Geral**

Nunca utilizar acentos, caracteres especiais e “ç” para denominar pastas, classes, métodos, variáveis, objetos e arquivos.

**Padrões de escrita: Objetos**

Nomes dos objetos devem ser exatamente os mesmos nomes de suas classes, iniciando com letra minúscula, ex:

```
LoginPage (classe) loginPage (objeto)
LoginFlows (classe) loginFlows (objeto)
```
=============================================================================================
O projeto ficou estruturado da seguinte forma:

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\Bases
PageBase (Contém as ações das pages: como Click, Sendkeys e Gettext)
TestBase (Contém as ações para executar os testes) 
	-O [SetUp], o qual executará ações antes de executar o método de teste, como por 
	exemplo preparar a massa de dados no Banco de Dados e iniciar a instância do driver.
	-O [TearDown], o qual executará ações após a execução do método de teste,
	como por exemplo finalizar a instância do driver.

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\DataBaseSteps
	Contém as ações que serão executadas no banco de dados, para preparar a massa de dados.

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\Flows
	Contém as ações que serão executadas várias vezes.

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\Pages
	-CriarTarefaPage.cs
	Contém o mapeamento e as ações que serão executadas na página de criar tarefas.

	-GerenciarCamposPersonalizadosPage.cs
	Contém o mapeamento e as ações que serão executadas na página de gerenciar campos personalizados.

	-GerenciarMarcadoresPage.cs
	Contém o mapeamento e as ações que serão executadas na página de gerenciar marcadores.

	-GerenciarPage.cs
	Contém o mapeamento e as ações que serão executadas na página de gerenciar.

	-GerenciarProjetosPage.cs
	Contém o mapeamento e as ações que serão executadas na página de gerenciar projetos.

	-GerenciarUsuariosPage.cs
	Contém o mapeamento e as ações que serão executadas na página de gerenciar usuários.

	-LoginPage.cs
	Contém o mapeamento e as ações que serão executadas na página de login.

	-MainPage.cs
	Contém o mapeamento e as ações que serão executadas na página principal.

	-MinhaVisaoPage.cs
	Contém o mapeamento e as ações que serão executadas na página minha visão.

	-VerDetalhesDaTarefaPage.cs
	Contém o mapeamento e as ações que serão executadas na página de ver detalhes da tarefa.

	-VerTarefasPage.cs
	Contém o mapeamento e as ações que serão executadas na página de ver tarefas.


- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\Queries
	-GerenciarQueries
	Contém as queries de SQL executadas nos testes de gerenciar.

	-LimparDadosBanco
	Contém as queries de SQL executadas para limpar os dados do banco de dados.

	-TarefaQueries
	Contém as queries de SQL executadas nos testes de tarefas.

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\Tests
	-CriarTarefaDataDrivenTests.cs
	Responsável pela execução do teste de criar tarefa utilizando o Data Driven.

	-CriarTarefaTests.cs
	Responsável pela execução dos testes da rotina de criar tarefas.

	-GerenciarCamposPersonalizadosTests.cs
	Responsável pela execução dos testes da rotina de campos personalizados.

	-GerenciarMarcadoresTests.cs
	Responsável pela execução dos testes da rotina de gerenciar marcadores.

	-GerenciarProjetosTests.cs
	Responsável pela execução dos testes da rotina de gerenciar projetos.

	-GerenciarUsuariosTests.cs
	Responsável pela execução dos testes da rotina de gerenciar usuários.

	-LoginTests.cs
	Responsável pela execução dos testes de login. (Testes executados com JavaScript)

	-VerTarefasTests.cs
	Responsável pela execução dos testes da rotina de ver tarefas.

- Mantis\CSharpSeleniumExtentReportNetCoreTemplate\appsettings.json
Responsável por gerenciar os parâmetros de execução dos testes.

	"BROWSER": "chrome", (browser a ser executado)
  "EXECUTION": "local", (tipo de execução, local ou remota)
  "DEFAULT_TIMEOUT_IN_SECONDS": "30",
  "HEADLESS": "false",
  "SELENIUM_HUB": "http://localhost:4444/wd/hub",

  "DB_URL": "localhost",
  "DB_PORT": "3306", (porta utilizada pelo banco de dados)
  "DB_NAME": "bugtracker", (nome do banco de dados)
  "DB_USER": "root", (usuário do banco de dados)
  "DB_PASSWORD": "root", (senha do banco de dados)
  "DB_CONNECTION_TIMEOUT": "50",

  "REPORT_NAME": "CSharpNetCoreReport", (nome do relatório de execução dos testes)

  "DEFAULT_APPLICATION_URL": "http://127.0.0.1:8989/login_page.php", (Url da aplicação a ser testada)
  "GET_SCREENSHOT_FOR_EACH_STEP": "true"


- Utilizado o ExtentReport para gerar o relatório de teste com screenshots.
Relatórios são armazenados em: CSharpSeleniumExtentReportNetCoreTemplate\bin\Debug\netcoreapp3.1\Reports
Evidências do relatório:
https://i.imgur.com/1nHYouX.jpg
https://i.imgur.com/FVm8u9s.jpg


- Evidências da execução dos testes na Azure
Testes em execução: https://i.imgur.com/KfiOJVd.jpg
Testes finalizados: https://i.imgur.com/GXyuHzb.jpg
Relatório de testes: https://i.imgur.com/6bK8ljF.jpg

- Evidências da execução dos testes utilizando Selenium GRID
Chrome: https://i.imgur.com/K1oGxoZ.jpg
Firefox: https://i.imgur.com/vHQXLak.jpg


